Aplikacja wykonana na zaliczenie projektu z przedmiotu Programowanie Urządzeń przenośnych

Wykorzystane technologie do stworzenia aplikacji:

* Expo: [Link](https://expo.io/) 
* React Native: [Link](https://facebook.github.io/react-native/)
* react-native-elements: [Link](https://react-native-training.github.io/react-native-elements/)
* react-geocode: [Link](https://www.npmjs.com/package/react-geocode) 
 
