import React from 'react';
import { 
	StyleSheet,
	Text,
	View,
	TextInput,
	TouchableOpacity,
	Platform,
	ScrollView } from 'react-native';
import { Constants, Location, Permissions } from 'expo';
import Geocode from "react-geocode";
import { Button, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';





export default class App extends React.Component {
	constructor(props) {
    super(props);
  }
    state = { 
    	postal_code: '',
    	location: null,
    	errorMessage: null,
    	info: [],
    	powiadomienie: 'Proszę Czekać',
    	wysokosc: '',
    	szerokosc: '',
    	country : '',
			state : '',
			city : '',
			postal : '',
			street : '',
			s_number : '',
			mode_changer: '1',
    };

    componentWillMount() {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
  };

  async checkPostal(){
  	this.setState({mode_changer: '1'})
  	var json = await fetch('http://kodypocztoweapi.pl/json/' + this.state.postal_code);
  	var responseJson = await json.json();
  	this.setState({info: responseJson})
  }
	async pozycja(){
		this.setState({mode_changer: '2'})
		if (this.state.errorMessage) {
	    this.setState({powiadomienie: this.state.errorMessage});
	  } else if (this.state.location) {
	  	this.setState({szerokosc: JSON.stringify(this.state.location.coords.latitude)})
	  	this.setState({wysokosc: JSON.stringify(this.state.location.coords.longitude)})

			Geocode.setApiKey("xxx"); //usunięte ze względów bezpieczeństwa
	  	
	  	Geocode.fromLatLng(this.state.location.coords.latitude, this.state.location.coords.longitude).then(
			  response => {
			    const country = response.results[0].address_components[5].long_name;
			    const state = response.results[0].address_components[4].long_name;
			    const city = response.results[0].address_components[2].long_name;
			    const postal = response.results[0].address_components[6].long_name;
			    const street = response.results[0].address_components[1].long_name;
			    const s_number = response.results[0].address_components[0].long_name;
			    this.setState({
				    country: country,
				    state: state,
				    city: city,
				    postal: postal,
				    street: street,
				    s_number: s_number,
			    })
			  },
			  error => {
			    console.error(error);
			  }
			);
	  	
	  }
 	 }
	render() {
			let mode_changer = this.state.mode_changer;
			let display;
			const infoalocation = <Text style={styles.loactionText}>Wysokość: {this.state.wysokosc}{"\n"}Szerokość: {this.state.szerokosc}{"\n"}Państwo: {this.state.country}{"\n"}Województwo: {this.state.state}{"\n"}Miasto: {this.state.city}{"\n"}Kod Pocztowy: {this.state.postal}{"\n"}Ulica: {this.state.street} {this.state.s_number}</Text>;

			if (mode_changer == "1") {
				if (this.state.info == '') {
					display = <Text style={styles.warning}>Nie wprowadzono kodu pocztowego lub kod pocztowy jest błędny</Text>
				}else{
				display = this.state.info.map((i, k) => <Text style={styles.dataText} key={k}>{i.gmina}  {i.miejscowosc};</Text>);	
				}
			} else if (mode_changer == "2") {
				display = infoalocation
			}
		return (
			<View style={styles.container}>
				<Input
					keyboardType='numeric'
					onChangeText={(text) => this.setState({postal_code: text})}
					value={this.state.postal_code}
					placeholder='Kod Pocztowy'
					/>
				<Button
      		large
      		raised
      		style={styles.button}
					onPress={this.checkPostal.bind(this)}
			  	icon={
				    <Icon
				      name='search'
				      size={15}
				      color='white'
				    />
				  }
				  buttonStyle={{
				  	marginTop: 5,
				    backgroundColor: "rgba(92, 99,216, 1)",
				    width: 300,
				    height: 45,
				    borderColor: "transparent",
				    borderWidth: 0,
				    borderRadius: 5
				  }}
			  	title='Sprawdź Kod Pocztowy' />
      	<Button
			  	large
			  	raised
			  	style={styles.button}
      		onPress={this.pozycja.bind(this)}
			  	icon={
				    <Icon
				      name='search'
				      size={15}
				      color='white'
				    />
				  }
				  buttonStyle={{
				    backgroundColor: "rgba(92, 99,216, 1)",
				    width: 300,
				    height: 45,
				    borderColor: "transparent",
				    borderWidth: 0,
				    borderRadius: 5,
				    marginBottom: 10,
				  }}
			  	title='Znajdź Gdzie Jestem' />
 				<View>
 				<ScrollView>
				  {display}
				</ScrollView>
       	</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		marginTop: 5,
		flex: 1,
		paddingTop: Expo.Constants.statusBarHeight,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'flex-start',
	},
	warning:{
		marginLeft: 10,
		marginRight: 10,
		fontSize: 16,
	},
	dataText: {
		marginLeft: 10,
		marginRight: 10,
		fontSize: 16,
	},
	loactionText:{
		marginLeft: 10,
		marginRight: 10,
		fontSize: 16,
	}
});
